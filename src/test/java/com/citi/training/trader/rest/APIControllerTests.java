package com.citi.training.trader.rest;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trader.model.Trade;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"h2", "no-scheduled"})
public class APIControllerTests {

    private static final Logger logger =
                        LoggerFactory.getLogger(APIControllerTests.class);

    @Autowired
    private TestRestTemplate restTemplate;

    private static final String apiBasePath = "/api/v1/status";

    private String testResponse = "status OK";
    @Test
    public void checkAPILink() {
    	ResponseEntity<String> checkResponse = restTemplate.exchange(apiBasePath, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});
    	logger.info("Body response: " + checkResponse.getBody());
    	assertEquals(testResponse, checkResponse.getBody());
    	
    }
}
