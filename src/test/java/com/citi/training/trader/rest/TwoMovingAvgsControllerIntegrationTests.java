package com.citi.training.trader.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.TwoMovingAvgs;

/**
 * Integration test for Stock REST Interface.
 *
 * Makes HTTP requests to {@link com.citi.training.stockr.rest.StockController}.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles({ "h2", "no-scheduled" })
public class TwoMovingAvgsControllerIntegrationTests {

	private static final Logger logger = LoggerFactory.getLogger(TwoMovingAvgsControllerIntegrationTests.class);

	@Autowired
	private TestRestTemplate restTemplate;

	private static final String twoMovingAvgsBasePath = "/api/v1/averages";

	@Test
	public void findAll() {

		ResponseEntity<List<TwoMovingAvgs>> findAllResponse = restTemplate.exchange(twoMovingAvgsBasePath,
				HttpMethod.GET, null, new ParameterizedTypeReference<List<TwoMovingAvgs>>() {
				});

		assertEquals(HttpStatus.OK, findAllResponse.getStatusCode());

		for (TwoMovingAvgs avg : findAllResponse.getBody()) {
			logger.info("Found TwoMovingAvgs: " + avg);
		}

		assertTrue("TwoMovingAvgs list should be greater than zero as src/test/resources/data.sql should be loaded",
				findAllResponse.getBody().size() > 0);

	}

//	@Test
//	public void deleteById_deletes() {
//
//		ResponseEntity<TwoMovingAvgs> createdResponse = restTemplate.getForEntity(twoMovingAvgsBasePath, TwoMovingAvgs.getId()
//				TwoMovingAvgs.class);
//
//		assertEquals(createdResponse.getStatusCode(), HttpStatus.CREATED);
//
//		TwoMovingAvgs foundAvgs = restTemplate
//				.getForObject(twoMovingAvgsBasePath + "/" + createdResponse.getBody().getId(), TwoMovingAvgs.class);
//
//		logger.info("Before delete, findById gives: " + foundAvgs);
//		assertNotNull(foundAvgs);
//
//		restTemplate.delete(twoMovingAvgsBasePath + "/" + createdResponse.getBody().getId());
//
//		ResponseEntity<TwoMovingAvgs> response = restTemplate.exchange(
//				twoMovingAvgsBasePath + "/" + createdResponse.getBody().getId(), HttpMethod.GET, null,
//				TwoMovingAvgs.class);
//
//		logger.info("After delete, findById response code is: " + response.getStatusCode());
//		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
//	}
}
