package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

public class TwoMovingAvgsTest {

    private int testId = 444;
    private String testTicker = "NFLX";
    private Stock testStock = new Stock(12, "NFLX");
    private int testSize = 2894;
    private double testShortAvg = 40;
    private double testLongAvg = 120;
    private double testExitProfitLoss = 100.99;
    private int testCurrentPosition = 1;
    private double testLastTradePrice = 19.99;
    private double testProfit = 40.99;
    private Date testStopped = new Date();

    @Test
    public void test_Strategy_fullConstructor() {
        TwoMovingAvgs testMovingAvgsStrategy = new TwoMovingAvgs(
                                testId, testStock, testSize, testExitProfitLoss,
                                testCurrentPosition, testLastTradePrice, testShortAvg, testLongAvg, testProfit,
                                testStopped);

        assertEquals("testStrategy should have id that was set in constructor",
                     testId, testMovingAvgsStrategy.getId());

        assertEquals("testStrategy should have Stock ticker that was set in constructor",
                     testTicker, testMovingAvgsStrategy.getStock().getTicker());

        assertEquals("testStrategy should have size that was set in constructor",
                     testSize, testMovingAvgsStrategy.getSize());

        assertEquals("testStrategy should have exit P&L that was set in constructor",
                     testExitProfitLoss, testMovingAvgsStrategy.getExitProfitLoss(), 0.0001);

        assertTrue("testStrategy should have Long Position",
                   testMovingAvgsStrategy.hasLongPosition());

        assertTrue("testStrategy should have Long Position",
                   testMovingAvgsStrategy.hasPosition());

        assertEquals("testStrategy should have lastTradePrice that was set in constructor",
                     testLastTradePrice, testMovingAvgsStrategy.getLastTradePrice(), 0.0001);
        
        assertEquals("testStrategy should have short avg that was set in constructor",
                testShortAvg, testMovingAvgsStrategy.getShortAvg(), 0.0001);
        
        assertEquals("testStrategy should have long avg that was set in constructor",
                testLongAvg, testMovingAvgsStrategy.getLongAvg(), 0.0001);
        
        
    }
}
