package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TradeTests {

    private int defaultId = -1;
    private double testPrice = 99.99;
    private int testTradeSize = 1500;
    private Trade.TradeType testTradeType = Trade.TradeType.BUY;
    private TwoMovingAvgs testSimpleStrategy = new TwoMovingAvgs();

    @Test
    public void test_Trade_constructor() {
        Trade testTrade = new Trade(testPrice, testTradeSize,
                                    testTradeType, testSimpleStrategy);

        assertEquals("Create Trade should contain default id of -1",
                     defaultId, testTrade.getId().intValue());

        assertEquals("Created Trade should contain price given in constructor",
                     testPrice, testTrade.getPrice(), 0.00001);

        assertEquals("Created Trade should contain size given in constructor",
                     testTradeSize, testTrade.getSize());

        assertEquals("Created Trade should contain trade type given in constructor",
                     testTradeType, testTrade.getTradeType());
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_TradeType_fromXmlString() {
        assertEquals("The XML String \"true\" should be converted to TradeType BUY",
                     Trade.TradeType.BUY, Trade.TradeType.fromXmlString("true"));

        assertEquals("The XML String \"false\" should be converted to TradeType SELL",
                     Trade.TradeType.SELL, Trade.TradeType.fromXmlString("false"));
    
        Trade.TradeType.fromXmlString("TTRUE");
    }
}
