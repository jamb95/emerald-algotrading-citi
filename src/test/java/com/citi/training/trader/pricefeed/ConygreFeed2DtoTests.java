package com.citi.training.trader.pricefeed;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;
import org.springframework.http.HttpStatus;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

public class ConygreFeed2DtoTests {
	
	private String testSymbol = "AAPL";
	private double testPrice = 99.67;
	private String testTimeStamp = "16:45:00";
	private String testCompanyName = "Apple";
	private int testPeriodNumber = 5;
	
	private static final String  DATE_FORMAT = "hh:mm:ss";
	
	@Test
	public void test_ConygreFeed2Dto_GettersAndSetters() {
		ConygreFeed2Dto conygreFeed2dto = new ConygreFeed2Dto();
		
		conygreFeed2dto.setSymbol(testSymbol);
		conygreFeed2dto.setPrice(testPrice);
		conygreFeed2dto.setTimeStamp(testTimeStamp);
		conygreFeed2dto.setCompanyName(testCompanyName);
		conygreFeed2dto.setPeriodNumber(testPeriodNumber);
		
		assertEquals("ConygreFeed2Dto symbol should be equal to testSymbol",
				testSymbol, conygreFeed2dto.getSymbol());
		assertEquals("ConygreFeed2Dto price should be equal to testPrice",
				testPrice, conygreFeed2dto.getPrice(), 0.001);
		assertEquals("ConygreFeed2Dto timeStamp should be equal to testTimeStamp",
				testTimeStamp, conygreFeed2dto.getTimeStamp());
		assertEquals("ConygreFeed2Dto companyName should be equal to testCompanyName",
				testCompanyName, conygreFeed2dto.getCompanyName());
		assertEquals("ConygreFeed2Dto periodNumber should be equal to testPeriodNumber",
				testPeriodNumber, conygreFeed2dto.getPeriodNumber());
	}
	
	   @Test
	    public void test_ConygreFeed2Dto_toString() {
		   ConygreFeed2Dto conygreFeed2dto = new ConygreFeed2Dto();
		   
			conygreFeed2dto.setSymbol(testSymbol);
			conygreFeed2dto.setPrice(testPrice);
			conygreFeed2dto.setTimeStamp(testTimeStamp);
			conygreFeed2dto.setCompanyName(testCompanyName);
			conygreFeed2dto.setPeriodNumber(testPeriodNumber);

	        assertTrue("toString should contain testSymbol",
	        		conygreFeed2dto.toString().contains(testSymbol));
	        assertTrue("toString should contain testPrice",
	        		conygreFeed2dto.toString().contains(Double.toString(testPrice)));
	        assertTrue("toString should contain testTimeStamp",
	        		conygreFeed2dto.toString().contains(testTimeStamp));
	        assertTrue("toString should contain testCompanyName",
	        		conygreFeed2dto.toString().contains(testCompanyName));
	        assertTrue("toString should contain testPeriodNumber",
	        		conygreFeed2dto.toString().contains(Integer.toString(testPeriodNumber)));
	    }

	   @Test
	   public void test_ConygreFeed2Dto_toPriceAndEquals() throws Exception {
		   ConygreFeed2Dto conygreFeed2dto = new ConygreFeed2Dto() {
			   private static final String  DATE_FORMAT = "hh:mm:ss";
		   };
		   
			conygreFeed2dto.setSymbol(testSymbol);
			conygreFeed2dto.setPrice(testPrice);
			conygreFeed2dto.setTimeStamp(testTimeStamp);
			conygreFeed2dto.setCompanyName(testCompanyName);
			conygreFeed2dto.setPeriodNumber(testPeriodNumber);
		   
		   Date recordedAt = new SimpleDateFormat(DATE_FORMAT).parse(conygreFeed2dto.getTimeStamp());
		   Price testPrice = new Price(new Stock(-1, testSymbol), conygreFeed2dto.getPrice(), recordedAt);
		   
		   assertEquals("ConygreFeed2Dto toPrice should be equal to testPrice",
				   testPrice, conygreFeed2dto.toPrice());
	   }
	   
}
