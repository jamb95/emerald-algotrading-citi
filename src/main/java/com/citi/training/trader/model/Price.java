package com.citi.training.trader.model;

import java.util.Date;

/**
 * This class represents the model for the Price object.
 * 
 * @author Team Emerald
 *
 */
public class Price {

	private Stock stock;
	private double price;
	private Date recordedAt;

	public Price(Stock stock, double price, Date recordedAt) {
		this.stock = stock;
		this.price = price;
		this.recordedAt = recordedAt;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getRecordedAt() {
		return recordedAt;
	}

	public void setRecordedAt(Date recordedAt) {
		this.recordedAt = recordedAt;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Price [stock=" + stock + ", price=" + price + ", recordedAt=" + recordedAt + "]";
	}
}