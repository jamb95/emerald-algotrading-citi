package com.citi.training.trader.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * A model object to hold the data for an instance of the SimpleStrategy algorithm.
 *
 * This strategy will trade for maxTrades and then exit.
 * 
 * See {@link com.citi.training.trader.strategy.TwoMovingAvgsAlgo}.
 *
 * @author Team Emerald
 *
 */
@Entity
public class TwoMovingAvgs {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    private Stock stock;
    private int size;
    private String strategyName;
    private double exitProfitLoss;
    private int currentPosition;
    private double lastTradePrice;
    private double shortAvg;
    private double longAvg;
    private double profit;
    private Date stopped;

    public TwoMovingAvgs() {}

    
  


	public TwoMovingAvgs(int id, int size, double exitProfitLoss, int currentPosition, double shortAvg,
			double longAvg) {
		super();
		this.id = id;
		this.size = size;
		this.exitProfitLoss = exitProfitLoss;
		this.currentPosition = currentPosition;
		this.shortAvg = shortAvg;
		this.longAvg = longAvg;
	}





	public TwoMovingAvgs(int id, Stock stock, int size,
                          double exitProfitLoss, int currentPosition,
                          double lastTradePrice,double shortAvg,double longAvg, double profit,
                          Date stopped) {
        this.id = id;
        this.stock = stock;
        this.size = size;
        this.exitProfitLoss = exitProfitLoss;
        this.currentPosition = currentPosition;
        this.lastTradePrice = lastTradePrice;
        this.shortAvg = shortAvg;
        this.longAvg = longAvg;
        this.profit = profit;
        this.stopped = stopped;
        this.strategyName = "Two Moving Averages";
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public double getExitProfitLoss() {
        return exitProfitLoss;
    }

    public void setExitProfitLoss(double exitProfitLoss) {
        this.exitProfitLoss = exitProfitLoss;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    public double getLastTradePrice() {
        return lastTradePrice;
    }

    public void setLastTradePrice(double lastTradePrice) {
        this.lastTradePrice = lastTradePrice;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    
    public double getShortAvg() {
		return shortAvg;
	}

	public void setShortAvg(double shortAvg) {
		this.shortAvg = shortAvg;
	}

	public double getLongAvg() {
		return longAvg;
	}

	public void setLongAvg(double longAvg) {
		this.longAvg = longAvg;
	}

	public Date getStopped() {
        return stopped;
    }

    public void setStopped(Date stopped) {
        this.stopped = stopped;
    }

    public void addProfitLoss(double profitLoss) {
        this.profit += profitLoss;
        if(Math.abs(this.profit) >= exitProfitLoss) {
            this.setStopped(new Date());
        }
    }

    public void stop() {
        this.stopped = new Date();
    }

    public boolean hasPosition() {
        return this.currentPosition != 0;
    }

    public boolean hasShortPosition() {
        return this.currentPosition < 0;
    }

    public boolean hasLongPosition() {
        return this.currentPosition > 0;
    }

    public void takeShortPosition() {
        this.currentPosition = -1;
    }

    public void takeLongPosition() {
        this.currentPosition = 1;
    }

    public void closePosition() {
        this.currentPosition = 0;
    }

    @Override
    public String toString() {
        return "Strategy [id=" + id + ", stock=" + stock +
                ", size=" + size + ", maxTrades=" + exitProfitLoss + 
                 ", Short Time=" + shortAvg + ", Long Time=" + longAvg +
                ", currentPosition=" + currentPosition +
                ", stopped=" + stopped + "]";
    }
}
