package com.citi.training.trader.service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.TwoMovingAvgsDao;

import com.citi.training.trader.model.TwoMovingAvgs;
import com.google.common.collect.Lists;

@Component
public class TwoMovingAvgsService {

	@Autowired
	private TwoMovingAvgsDao twoMovingAvgsDao;

	public List<TwoMovingAvgs> findAll() {
		return Lists.newArrayList(twoMovingAvgsDao.findAll());
	}

	public List<TwoMovingAvgs> findActive() {
		List<TwoMovingAvgs> allStrategies = findAll();
		Iterator listIterator = allStrategies.listIterator();

		while (listIterator.hasNext()) {
			TwoMovingAvgs strategy = (TwoMovingAvgs) listIterator.next();
			Date currentDateAndTime = new Date();
			if (strategy.getStopped().compareTo(currentDateAndTime) < 0) {
				listIterator.remove();
			}
		}

		return allStrategies;
	}

	public TwoMovingAvgs save(TwoMovingAvgs strategy) {
		return twoMovingAvgsDao.save(strategy);
	}

	public void deleteById(int id) {
		twoMovingAvgsDao.deleteById(id);
	}
}
