package com.citi.training.trader.rest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.Stock;

/**
 * REST Controller for {@link com.citi.training.trader.model.Stock} resource.
 *
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/status")
public class APIStatusController {



	@RequestMapping( method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	public String checkAPILink() {	
		return "status OK";
	}
}
