package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.TwoMovingAvgs;
import com.citi.training.trader.service.TwoMovingAvgsService;

/**
 * REST Controller for {@link com.citi.training.trader.model.SimpleStrategy}
 * resource.
 *
 */
@CrossOrigin(origins = "${com.citi.training.trader.cross-origin-host:http://localhost:4200}")
@RestController
@RequestMapping("/api/v1/averages")
public class MovingAvgController {

	private static final Logger LOG = LoggerFactory.getLogger(MovingAvgController.class);

	@Autowired
	private TwoMovingAvgsService movingAvgsService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<TwoMovingAvgs> findAll() {
		LOG.info("findAll()");
		return movingAvgsService.findAll();
	}

	@RequestMapping(value = "/active", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<TwoMovingAvgs> findActive() {
		LOG.info("findActive()");
		return movingAvgsService.findActive();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable int id) {
		LOG.info("deleteById [" + id + "]");
		movingAvgsService.deleteById(id);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<TwoMovingAvgs> create(@RequestBody TwoMovingAvgs twoMovingAvgs) {
		LOG.info("HTTP POST : create[" + twoMovingAvgs + "]");

		TwoMovingAvgs createdStrategy = movingAvgsService.save(twoMovingAvgs);
		LOG.info("created createdStrategy: [" + createdStrategy + "]");

		return new ResponseEntity<TwoMovingAvgs>(createdStrategy, HttpStatus.CREATED);
	}
}