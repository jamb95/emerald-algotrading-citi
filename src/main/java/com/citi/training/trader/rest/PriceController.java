package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.service.PriceService;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/pricing")
public class PriceController {

	
	@Autowired
	private PriceService priceService;
	
	private static final Logger LOG = LoggerFactory.getLogger(PriceController.class);

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Object> findAvailableTickers() {
		LOG.info("findAvailableTickers()");
		return priceService.findAvailableTickers();
	}
}