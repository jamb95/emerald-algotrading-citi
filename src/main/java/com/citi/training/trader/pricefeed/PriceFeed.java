package com.citi.training.trader.pricefeed;

import java.util.List;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

/**
 * This interface is used to find latest prices and available tickers from the utilised price feed. 
 * 
 * @author Team Emerald
 *
 */
public interface PriceFeed {

	 List<Price> findLatest(Stock stock, int count);
	 
	 List<Object> findAvailableTickers();
}
