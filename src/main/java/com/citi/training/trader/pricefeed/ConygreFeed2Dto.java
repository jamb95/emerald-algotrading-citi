package com.citi.training.trader.pricefeed;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citi.training.trader.exceptions.EntityNotFoundException;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

/**
 * This class is used to parse the raw data coming from the ConygreFeed2 into a list of Price objects.
 * 
 * @author Team Emerald
 *
 */
public class ConygreFeed2Dto {

    private static final String  DATE_FORMAT = "hh:mm:ss";
    private static final Logger LOG = LoggerFactory.getLogger(ConygreFeed2Dto.class);

    private String symbol;
    private double price;
    private String timeStamp;
    private String companyName;
    private int periodNumber;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getPeriodNumber() {
        return periodNumber;
    }

    public void setPeriodNumber(int periodNumber) {
        this.periodNumber = periodNumber;
    }

    public Price toPrice() {
        try {
            Date recordedAt = new SimpleDateFormat(DATE_FORMAT).parse(this.timeStamp);

            return new Price(new Stock(-1, this.symbol),
                             this.price,
                             recordedAt);
        } catch(ParseException ex) {
            String msg = "Failed to parse Date format [" + DATE_FORMAT +
                         "] from price feed trade Time: [" + this.timeStamp + "]";

            LOG.error(msg);
            throw new EntityNotFoundException(msg);
        }
    }

    @Override
    public String toString() {
        return "ConygreFeed2Dto [symbol=" + symbol + ", price=" + price +
               ", timeStamp=" + timeStamp + ", companyName=" + companyName +
               ", periodNumber=" + periodNumber + "]";
    }
}

