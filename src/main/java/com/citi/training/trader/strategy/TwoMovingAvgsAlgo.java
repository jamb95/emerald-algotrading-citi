package com.citi.training.trader.strategy;


import java.util.ArrayList;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.exceptions.EntityNotFoundException;
import com.citi.training.trader.messaging.TradeSender;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.TwoMovingAvgs;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.service.PriceService;
import com.citi.training.trader.service.TwoMovingAvgsService;
import com.citi.training.trader.service.TradeService;

@Profile("!no-scheduled")
@Component
public class TwoMovingAvgsAlgo implements StrategyAlgorithm {

	private static final Logger logger = LoggerFactory.getLogger(TwoMovingAvgsAlgo.class);

	@Autowired
	private TradeSender tradeSender;

	@Autowired
	private PriceService priceService;

	@Autowired
	private TwoMovingAvgsService strategyService;

	@Autowired
	private TradeService tradeService;

	private List<Double> historicalAverages = new ArrayList<>();
	
	private Double strategyProfit = 0.0;
	
	@Scheduled(fixedRateString = "${simple.strategy.refresh.rate_ms:5000}")
	public void run() {
		double buyPrice;


		for (TwoMovingAvgs strategy : strategyService.findAll()) {

			Trade lastTrade = null;
			try {
				lastTrade = tradeService.findLatestByStrategyId(strategy.getId());
			} catch (EntityNotFoundException ex) {
				logger.debug("No Trades for strategy id: " + strategy.getId());
			}

			if (lastTrade != null) {
				if (lastTrade.getState() == TradeState.WAITING_FOR_REPLY) {
					logger.debug("Waiting for last trade to complete, do nothing");
					continue;
				}

			}

			// TODO: We should be checking for open positions and
			// closing them if necessary
			if (strategy.getStopped() != null) {
				continue;
			}

			logger.debug("Taking strategic action");
			int shortTime = (int) Math.round(strategy.getShortAvg());
			int longTime = (int) Math.round(strategy.getLongAvg());

			double shortAvg = getAvg(strategy, shortTime);
			double longAvg = getAvg(strategy, longTime);
			
			historicalAverages.add(shortAvg);
			historicalAverages.add(longAvg);
			
			if (historicalAverages.size() < 4) {
				logger.debug("Not enough averages stored, taking no action");
				continue;
			}
			
			if (!strategy.hasPosition()) {
				logger.debug("Strategy has no position");

				if (shortAvg == longAvg) {
					logger.debug("Insufficient price change, taking no action");
					continue;
				} else if (makeTradeDecision(historicalAverages) == "HOLD" ) { 
					logger.debug("Analysing next trades, taking no action");
					continue;
				} else if (makeTradeDecision(historicalAverages) == "BUY") {
					logger.debug("Trading to open long position for strategy: " + strategy);
					double lastTradePrice = makeTrade(strategy, Trade.TradeType.BUY);
					strategy.setLastTradePrice(lastTradePrice);
					strategy.takeLongPosition();
				} else if (makeTradeDecision(historicalAverages) == "SELL") {
					logger.debug("Trading to open short position for strategy: " + strategy);
					double lastTradePrice = makeTrade(strategy, Trade.TradeType.SELL);
					strategy.setLastTradePrice(lastTradePrice);
					strategy.takeShortPosition();
				}

			} else if (strategy.hasLongPosition()) {
				// we have a long position => close the position by selling
				if (makeTradeDecision(historicalAverages) == "BUY") {
					logger.debug("We have a long position, waiting to sell for strategy: " + strategy);
					continue;
				} else if (makeTradeDecision(historicalAverages) == "SELL") {
					logger.debug("Trading to close long position for strategy: " + strategy);
					double tradeAmount = makeTrade(strategy, Trade.TradeType.SELL);
					closePosition(Trade.TradeType.SELL, strategy.getLastTradePrice(), tradeAmount, strategy);
				} else if (makeTradeDecision(historicalAverages) == "HOLD" ) { 
					logger.debug("Analysing next trades, taking no action");
					continue;
				}
			} else if (strategy.hasShortPosition()) {
				// we have a short position => close the position by buying
				if (makeTradeDecision(historicalAverages) == "BUY") {
					logger.debug("Trading to close short position for strategy: " + strategy);
					double tradeAmount = makeTrade(strategy, Trade.TradeType.BUY);
					closePosition(Trade.TradeType.BUY, strategy.getLastTradePrice(), tradeAmount, strategy);
				} else if (makeTradeDecision(historicalAverages) == "SELL") {
					logger.debug("We have a short position, waiting to buy for strategy: " + strategy);
					continue;
				}else if (makeTradeDecision(historicalAverages) == "HOLD" ) { 
					logger.debug("Analysing next trades, taking no action");
					continue;
				}
			}
			strategyService.save(strategy);
		}
			
	}


	private String makeTradeDecision(List<Double> averages) {
		double priceFourTradesAgo = averages.get(averages.size() - 4); // short avg
		double priceThreeTradesAgo = averages.get(averages.size() - 3); // long avg
		double priceTwoTradesAgo = averages.get(averages.size() - 2); // short avg
		double priceOneTradeAgo = averages.get(averages.size() - 1);  // long avg
		
		if (priceFourTradesAgo > priceThreeTradesAgo && priceOneTradeAgo >= priceTwoTradesAgo) {
			return "SELL";
		} else if (priceThreeTradesAgo > priceFourTradesAgo && priceTwoTradesAgo >= priceOneTradeAgo) {
			return "BUY";
		} 
		
		return "HOLD";
	}
	
	private void closePosition(Trade.TradeType tradeType, 
			double lastTradeStockPrice, double thisTradeStockPrice, TwoMovingAvgs strategy) {
		
		logger.info("last trade price: " + lastTradeStockPrice);
		logger.info("this trade price: " + thisTradeStockPrice);
		logger.info("strategy size: " + strategy.getSize());
		
		double totalProfitLoss = 0;
		
		if (tradeType == Trade.TradeType.BUY) {
			totalProfitLoss = (lastTradeStockPrice - thisTradeStockPrice)*strategy.getSize();
		} else if (tradeType == Trade.TradeType.SELL) {
			totalProfitLoss = (thisTradeStockPrice - lastTradeStockPrice)*strategy.getSize();
		}
		
		strategyProfit += totalProfitLoss;
		
		logger.info("strategyProfit price: " + strategyProfit);
		logger.info("totalProfitLoss price: " + totalProfitLoss);

		logger.debug("Recording profit/loss of: " + strategyProfit + " for strategy: " + strategy);
		strategy.addProfitLoss(strategyProfit);


		strategy.closePosition();
		
		if (Math.abs(strategyProfit) >= strategy.getExitProfitLoss()) {
			logger.debug("Exit condition reached, exiting strategy");
			strategy.setStopped(new Date());
			strategy.setLastTradePrice(lastTradeStockPrice);
			strategyService.save(strategy);
		}
	}

	private double makeTrade(TwoMovingAvgs strategy, Trade.TradeType tradeType) {
		logger.info("making trade: " + strategy);
		logger.info("trade type: " + tradeType);
		Price currentPrice = priceService.findLatest(strategy.getStock(), 1).get(0);
		tradeSender.sendTrade(new Trade(currentPrice.getPrice(), strategy.getSize(), tradeType, strategy));
		strategyService.save(strategy);
		return currentPrice.getPrice();
	}

	private double getAvg(TwoMovingAvgs strategy, int time) {
		// Assumption that theres a trade per sec
		double sum = 0;
		double avg = 0;
		List<Price> prices = priceService.findLatest(strategy.getStock(), time);

		for (int i = 0; i < time; i++) {
			sum += prices.get(i).getPrice();
		}

		logger.debug("Sum:" + System.out.format("%.5f", sum));
		logger.debug("Getting averages based on prices:");
		avg = sum / time;
		logger.debug("avg" + System.out.format("%.5f", avg));
		return avg;
	}

}
