package com.citi.training.trader.dao;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.trader.model.TwoMovingAvgs;

/**
 * This interface is used for generic CRUD operations on a repository 
 * for types TwoMovingAvgs and Integer.
 * 
 * @author Team Emerald
 *
 */
public interface TwoMovingAvgsDao extends CrudRepository<TwoMovingAvgs, Integer>{

}
