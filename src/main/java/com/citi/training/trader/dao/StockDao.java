package com.citi.training.trader.dao;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.trader.model.Stock;

/**
 * This interface is used for generic CRUD operations on a repository 
 * for types Stock and Integer.
 * 
 * @author Team Emerald
 *
 */
public interface StockDao extends CrudRepository<Stock, Integer> {

    Stock findByTicker(String ticker);

}
